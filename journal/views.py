"""
© Copyright 2022 Yves de Champlain

SPDX-License-Identifier: LiLiQ-Rplus-1.1
License-Filename: LICENCE
"""

import locale

from django.contrib.auth.models import User
from django.contrib.postgres.search import SearchVector
from django.shortcuts import redirect, render

from .models import Article, Issue, Keyword, User
from .forms import FormGeneralQuery

locale.setlocale(locale.LC_ALL, '')


def home(request, request_type=None, object_id=None):
    """Process General Query form

    Args:
        request: (HttpRequest object)
        request_type: 'auteur/parution/keyword' (Str)
        object_id: Object ID for specified type (Int)

    Returns:
        HttpResponse(index.html)

    """

    if request.method != 'GET':
        return redirect('home')

    message = None
    num_results = None
    results = None
    expression = ""

    if request_type is not None:
        num_results = 0
        if request_type == 'auteur':
            query = User.objects.get(id=object_id)
            expression = query.first_name + " " + query.last_name
            results = Article.available_objects.filter(authors__id=object_id)
        elif request_type == 'parution':
            query = Issue.available_objects.get(number=object_id)
            expression = str(query)
            results = Article.available_objects.filter(issue__number=object_id)
        elif request_type == 'keyword':
            query = Keyword.available_objects.get(id=object_id)
            expression = str(query)
            results = Article.available_objects.filter(keywords__id=object_id)

        form = FormGeneralQuery(initial={'Query': expression})

    else:   # Real query
        form = FormGeneralQuery(request.GET)

        if form.is_valid():
            query = form.cleaned_data['Query']
            if query:
                num_results = 0
                results = Article.available_objects.annotate(search=SearchVector('authors__first_name',
                                                                                 'authors__last_name',
                                                                                 'title',
                                                                                 'issue__number',
                                                                                 'issue__date_published',
                                                                                 'keywords__expression')).filter(search=query)
        else:
            message = form.errors

    # print(results)
    filtered_results = []
    if results is not None:
        parsed_results = []
        for result in results:
            if result.id not in parsed_results:
                filtered_results.append(result)
                parsed_results.append(result.id)
                num_results += 1

    context = {
        'form': form,
        'message': message,
        'num_results': num_results,
        'results': filtered_results,
    }

    return render(request, 'index.html', context)


def explore(request):
    """Process General Query form

    Args:
        request: (HttpRequest object)

    Returns:
        HttpResponse(index.html)

    """

    auteurs = User.objects.all().order_by('-occurrence')
    mots_cles = Keyword.available_objects.all().order_by('-occurrence')
    parutions = Issue.available_objects.all()

    annexes = Article.available_objects.filter(has_annexe=True)
    issue_annexes = []
    for annexe in annexes:
        issue_annexes.append(annexe.issue.number)

    context = {
        'auteurs': auteurs,
        'mots_cles': mots_cles,
        'parutions': parutions,
        'annexes': issue_annexes,
    }

    return render(request, 'explore.html', context)
