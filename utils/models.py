"""
© Copyright 2018-2021 Yves de Champlain, Stéphane Lapointe,
                      Abdoulaye Fall, James Caveen & Brad Covey (Code).
© Copyright 2009-2021 Lucien Aubé, Henri Boudreault, Céline Chatigny,
                      Yves de Champlain, Ilia Essopos & Chantal Lepire (Portfolio).

SPDX-License-Identifier: LiLiQ-Rplus-1.1
License-Filename: LICENCE
"""

from model_utils.models import SoftDeletableModel, StatusModel, TimeStampedModel
from model_utils import Choices

from django.utils.translation import gettext_lazy as _


class MetaData(TimeStampedModel, StatusModel, SoftDeletableModel):
    """Abstract class to implement TimeStamp, Status and SoftDelete in models

    TimeStampedModel:
        Attributes:
            created: (datetime) Self-updating
            modified: (datetime) Self-updating

    StatusModel:
        Attributes:
            Status: (Choice)
            status_modified: (Datetime) Self-updating

    SoftDeletableModel:
        Attributes:
            is_removed: (bool) Default = False, set True on delete

    """

    STATUS = Choices(
        ('Draft', _('Draft')),
        ('Submitted', _('Submitted')),
        ('Underreview', _('Underreview')),
        ('Reviewed', _('Reviewed')),
        ('Rejected', _('Rejected')),
        ('Final', _('Final')),
    )

    class Meta:
        abstract = True
