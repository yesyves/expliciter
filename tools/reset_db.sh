#/bin/sh

find . -path "*/migrations/*.py" -not -name "__init__.py" -not -path "./data/*" -delete
find . -path "*.pyc"  -delete

if [ -e sqlite.db ]
  then
    rm sqlite.db
fi

if [ -z $DB_NAME ]
  then
    DB_NAME=expliciter
fi

if [ -z $DB_USER ]
  then
    DB_USER=cert_tool
fi

if [ -n `which psql` ]
  then
    psql -U $DB_USER $DB_NAME -a -f tools/sql/drop_tables.sql
fi

python manage.py makemigrations
python manage.py migrate

echo "Load data from :"

for fix in superuser
  do
    echo $fix
    python manage.py loaddata $fix
done

python manage.py load_database
