from django.contrib import admin

from . import models


class UserAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'username', 'occurrence')


class IssueAdmin(admin.ModelAdmin):
    list_display = ('number', 'date_published')


class KeywordAdmin(admin.ModelAdmin):
    list_display = ('expression', 'occurrence')


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'issue', 'pages')


admin.site.register(models.User, UserAdmin)
admin.site.register(models.Issue, IssueAdmin)
admin.site.register(models.Keyword, KeywordAdmin)
admin.site.register(models.Article, ArticleAdmin)
