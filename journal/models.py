"""
© Copyright 2022 Yves de Champlain

SPDX-License-Identifier: LiLiQ-Rplus-1.1
License-Filename: LICENCE
"""

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models

from utils.models import MetaData


class User(AbstractUser):
    occurrence = models.IntegerField(default=1)

    class Meta:
        ordering = ['last_name', 'first_name']
        verbose_name = 'Auteur'
        verbose_name_plural = 'Auteurs'

    def __str__(self):
        this_author = ""
        this_author += self.last_name
        if self.first_name != "":
            this_author += ",&nbsp;" + self.first_name[0] + "."
        return this_author


class Issue(MetaData):
    number = models.IntegerField()
    date_published = models.DateField()
    hors_serie = models.BooleanField(default=False)

    class Meta:
        ordering = ['date_published', 'number']
        verbose_name = 'Parution'
        verbose_name_plural = 'Parutions'

    def __str__(self):
        return str(self.number) + " (" + self.date_published.strftime("%B %Y") + ")"


class Keyword(MetaData):
    expression = models.CharField(max_length=31)
    occurrence = models.IntegerField(default=1)

    class Meta:
        ordering = ['expression']
        verbose_name = 'Mot clé'
        verbose_name_plural = 'Mots clés'

    def __str__(self):
        return self.expression


class Article(MetaData):
    authors = models.ManyToManyField(User)
    title = models.TextField(max_length=255)
    issue = models.ForeignKey(Issue,
                              on_delete=models.PROTECT)
    pages = models.TextField(max_length=15)
    keywords = models.ManyToManyField(Keyword)
    has_annexe = models.BooleanField(default=False)

    class Meta:
        ordering = ['issue', 'pages']
        verbose_name = 'Article'
        verbose_name_plural = 'Articles'

    def __str__(self):
        these_authors = ""
        for author in self.authors.all():
            these_authors += str(author) + " "
        journal = "Expliciter"
        if self.issue.hors_serie:
            journal += " Hors série"
        return (
            these_authors
            + "(" + self.issue.date_published.strftime("%Y") + "). "
            + self.title + ". "
            + "<i>" + journal + "</i>, "
            + "(" + str(self.issue.number) + "), "
            + self.pages + "."
        )
