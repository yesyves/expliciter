"""
© Copyright 2018-2021 Yves de Champlain, Stéphane Lapointe,
                      Abdoulaye Fall, James Caveen & Brad Covey (Code).
© Copyright 2009-2021 Lucien Aubé, Henri Boudreault, Céline Chatigny,
                      Yves de Champlain, Ilia Essopos & Chantal Lepire (Portfolio).

SPDX-License-Identifier: LiLiQ-Rplus-1.1
License-Filename: LICENCE
"""

import json
import locale
import random
import string

from django.contrib.auth.decorators import login_required
from django.core.mail import mail_admins
from django.http import HttpResponse
# from django.shortcuts import redirect
from django.template import loader
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_exempt

# from portfolio.models import Achievement

locale.setlocale(locale.LC_ALL, '')


def get_random_string(length):
    """Return random lowercase ascii string of specified length

    Args:
        length: (int)

    Returns:
        String

    """
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str
