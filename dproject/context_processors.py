from django.contrib.sites.models import Site

from . import __version__, __version_info__


def app_info(request):
    return {
        'app_version': __version__,
        'version_info': __version_info__,
    }

def site_processor(request):
    return {'site': Site.objects.get_current()}
