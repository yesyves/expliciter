#/bin/sh

# Validation tools

# Flake8
flake8 > flakes.py
$EDITOR flakes.py

# # html-validator
# cd ~/Documents
# vnu/bin/java -cp vnu.jar nu.validator.servlet.Main 8888 &
# echo "HTML validator listening on port 8888"
# $EDITOR validationerrors/

# W3C css validator
# https://jigsaw.w3.org/css-validator/

# js validator
# atom-jshint
