from django import forms

class FormGeneralQuery(forms.Form):
    """
    General query form
    """

    Query = forms.CharField(
        label='Termes de la recherche',
        required=False,
    )
