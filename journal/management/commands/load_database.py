#  © Copyright Yves de Champlain et al. 2018-2022
#      For more informations, see https://doc.epassages.ca/index.php/contributions
#  SPDX-License-Identifier: LiLiQ-Rplus-1.1
#  License-Filename: LICENCE

import csv

from datetime import date

from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand

from journal.models import Article, Issue, Keyword, User


class Command(BaseCommand):
    help = 'Load database from ./sommaire.csv'

    def handle(self, *args, **options):

        print('\nLoading database ...\n')

        created_issues = 0
        created_users = 0
        updated_users = 0
        created_articles = 0
        inserted_annexes = 0
        created_keywords = 0
        updated_keywords = 0

        this_number = 0
        this_issue = None

        keywords = ['mc1', 'mc2', 'mc3', 'mc4', 'mc5']

        with open('sommaire.csv', newline='') as csvfile:
            fieldnames = ['No',
                          'Date',
                          'Auteurs',
                          'Titre',
                          'Pages',
                          'Annexe',
                          'Hors_serie',
                          'mc1',
                          'mc2',
                          'mc3',
                          'mc4',
                          'mc5']
            reader = csv.DictReader(csvfile, fieldnames=fieldnames, delimiter=';')

            for row in reader:
                # DEBUG:
                # print(row)

                no = row['No']
                if no == 'No':
                    continue

                if no != this_number:
                    this_number = no
                    if row['Hors_serie'] == 'Oui':
                        hors_serie = True
                    else:
                        hors_serie = False
                    this_issue = Issue(
                        number=no,
                        date_published=date.fromisoformat(row['Date'].replace('/', '-')),
                        hors_serie=hors_serie,
                    )
                    this_issue.save()
                    created_issues += 1
                    # DEBUG:
                    # print(f'{created_issues} issues created')

                this_article = Article(
                    title=row['Titre'],
                    issue=this_issue,
                    pages=row['Pages'],
                )
                if row['Annexe'] == 'Oui':
                    this_article.has_annexe = True
                    inserted_annexes += 1
                    # DEBUG:
                    # print(f'{inserted_annexes} annexes inserted')
                this_article.save()
                created_articles += 1
                # DEBUG:
                # print(f'{created_articles} article created')

                authors = row['Auteurs'].split(', ')
                for author in authors:
                    name = author.split()
                    try:
                        test = name[1]
                        username = name[1] + '.' + name[0]
                    except IndexError:
                        name.append('')
                        username = name[0]

                    try:
                        this_author = User.objects.get(username=username)
                        this_author.occurrence += 1
                        this_author.save()
                        updated_users += 1
                        # DEBUG:
                        # print(f'{updated_users} users updated')
                    except ObjectDoesNotExist:
                        this_author = User(
                            username=username,
                            first_name=name[1],
                            last_name=name[0].replace('_', ' ')
                        )
                        this_author.save()
                        created_users += 1
                        # DEBUG:
                        # print(f'{created_users} users created')
                    this_article.authors.add(this_author)

                for keyword in keywords:
                    expression = row[keyword]
                    if expression:
                        try:
                            this_keyword = Keyword.available_objects.get(expression=expression)
                            this_keyword.occurrence += 1
                            this_keyword.save()
                            updated_keywords += 1
                            # DEBUG:
                            # print(f'{updated_keywords} keywords updated')
                        except ObjectDoesNotExist:
                            this_keyword = Keyword(expression=expression)
                            this_keyword.save()
                            created_keywords += 1
                            # DEBUG:
                            # print(f'{created_keywords} keywords created')
                        this_article.keywords.add(this_keyword)
                    else:
                        break

        print(f'{created_issues} issues created')
        print(f'{created_users} users created')
        print(f'{updated_users} users updated')
        print(f'{created_articles} article created')
        print(f'{inserted_annexes} annexes inserted')
        print(f'{created_keywords} keywords created')
        print(f'{updated_keywords} keywords updated')

