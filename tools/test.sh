#!/bin/sh

coverage run manage.py test -v 2
coverage html
open -a Safari.app htmlcov/index.html
